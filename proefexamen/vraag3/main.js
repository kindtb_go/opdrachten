var gridSize = 10;

function emptyPattern() {
    var html = '';
    for (var i= 0; i<gridSize; i++) {
        for (var j=0; j<gridSize; j++) {
            html += '<div class="cell white"></div>';
        }
        html+= '<div class="newLine"></div>';
    }

    document.getElementById('raster').innerHTML = html;
}

emptyPattern();
