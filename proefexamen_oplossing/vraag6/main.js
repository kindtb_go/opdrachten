var cards = [];
var types = ['k','h','p','r'];
var cardNr = ['1','2','3','4','5','6','7','8','9','10','b','q','k'];
for (var i=0; i<types.length; i++) {
    for (var j=0; j<cardNr.length; j++) {
        cards.push(types[i]+'_'+cardNr[j]);
    }
}

function getCards() {
    var aantal = Math.floor(Math.random() * 6) + 1;
    var selectedCards = [];
    var selected = 0;
    while (selectedCards.length < aantal) {
        var cardId = Math.floor(Math.random() * 52);
        var found = false;
        for (var i=0; i<selectedCards.length; i++) {
            if (selectedCards[i] == cardId) found = true;
        }
        if (!found) selectedCards.push(cardId);
    }

    console.log(selectedCards);

    var html = '';
    for (i=0; i<selectedCards.length; i++) {
        html+= '<img src="cards/'+cards[selectedCards[i]]+'.png" style="padding:5px;padding-top:30px"/>';
    }
    document.getElementById('card_container').innerHTML = html;

}