var gridSize = 50;

function pattern(criteria) {
    var html = '';
    for (var i= 0; i<gridSize; i++) {
        for (var j=0; j<gridSize; j++) {
            html += '<div class="cell '+criteria(i,j)+'"></div>';
        }
        html+= '<div class="newLine"></div>';
    }
    document.getElementById('raster').innerHTML = html;
}
function emptyPattern() {
    pattern(function(i,j){return 'white'});
}
function pattern1() {
    pattern(function(i,j){return (i==j || i+j== 9 ) ? "black" : "white";});
}
function pattern2() {
    pattern(function(i,j){return (j%2==1) ? "black" : "white";});
}
function pattern3() {
    pattern(function(i,j){return ((i<5 && j<5) || (i>=5 && j>=5)) ? "black" : "white";});
}
function pattern4() {
    pattern(function(i,j){return (i+j<5 || 18-i-j<5 || 9+i-j<5 || 9-i+j<5) ? "black" : "white";});
}
function pattern5() {
    pattern(function(i,j){return (i==0 || i == 9 || j==0 || j == 9 || (((i==3) || (i==6)) && (j>2) && ((j<7))) || (((j==3) || (j==6)) && (i>2) && ((i<7)))) ? "black" : "white";});
}

emptyPattern();