function search() {
    var key = document.getElementById('search').value;
    var url = "http://musicbrainz.org/ws/2/recording?query=\"";
    if (key != "") {
        $.get( url + key + "\"&fmt=json", function( data ) {
            var recordings = [];
            for (var i=0; i<data.recordings.length; i++) {
                recordings.push({
                    'title': data.recordings[i].title,
                    'artist' : data.recordings[i]["artist-credit"][0].artist.name,
                    'date' : data.recordings[i]["releases"][0].date,
                    'country' : data.recordings[i]["releases"][0].country,
                    'album' : data.recordings[i]["releases"][0].title
                })
            }
            display(recordings);
        });
    }
}

function display(recordings) {
    var html = '<table class="table table-inverse">';
    html+= '<thead><tr><th>#</th><th>Title</th><th>Artist</th><th>Album</th><th>Date</th><th>Country</th></tr></thead>';
    for (var i=0; i<recordings.length; i++) {
        var recording = recordings[i];
        html+= '<tr><td>'+(i+1)+'</td><td>'+recording.title+'</td><td>'+recording.artist+'</td><td>'+recording.album+'</td><td>'+recording.date+'</td><td>'+recording.country+'</td></tr>';
    }
    html+= '</table>';
    document.getElementById('recordings').innerHTML = html
}