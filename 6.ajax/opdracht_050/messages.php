<?php
$berichten = array();
$data = file_get_contents('data.txt');
foreach (explode("\n",$data) as $line) {
    if ($line != '') {
        $parts = explode(',',$line);
        $berichten[] = array(
            'sender' => $parts[0],
            'message' => $parts[1]

        );
    }
}
echo json_encode($berichten);