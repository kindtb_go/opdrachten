<?php
$personen = array();
$personen[] = array("naam"=>"Jona", "leeftijd"=>17, "lengte"=>167,"gewicht"=>64);
$personen[] = array("naam"=>"Babs", "leeftijd"=>24, "lengte"=>172,"gewicht"=>68);
$personen[] = array("naam"=>"Liam", "leeftijd"=>3, "lengte"=>84,"gewicht"=>24);
$personen[] = array("naam"=>"Jules", "leeftijd"=>62, "lengte"=>169,"gewicht"=>82);
$personen[] = array("naam"=>"Greet", "leeftijd"=>47, "lengte"=>173,"gewicht"=>65);
$personen[] = array("naam"=>"Jason", "leeftijd"=>43, "lengte"=>178,"gewicht"=>94);
$personen[] = array("naam"=>"Imke", "leeftijd"=>34, "lengte"=>184,"gewicht"=>74);
$personen[] = array("naam"=>"Thibault", "leeftijd"=>31, "lengte"=>192,"gewicht"=>70);
$personen[] = array("naam"=>"Ben", "leeftijd"=>19, "lengte"=>181,"gewicht"=>79);
$personen[] = array("naam"=>"Stef", "leeftijd"=>20, "lengte"=>182,"gewicht"=>80);
echo json_encode($personen);